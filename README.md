# Issue Tracker API [![Build status](https://gitlab.com/simonbreiter/issue-tracker-api/badges/master/build.svg)](https://gitlab.com/simonbreiter/issue-tracker-api) [![License](http://img.shields.io/:license-mit-blue.svg)](http://doge.mit-license.org) [![Developed By](https://img.shields.io/badge/developed%20with%20♥%20by-Emanuele%20&%20Simon-red.svg)](https://emanuelemazzotta.com/) 

This repository is an API for an issue tracker service.

## Usage

Start dev environment:
```bash
yarn dev
```

Start server:
```bash
yarn start
```

## Authors

[Emanuele Mazzotta](mailto:hello@mazzotta.me)

[Simon Breiter](mailto:hello@simonbreiter.com)

## License

[MIT License](LICENSE.md) © Emanuele Mazzotta, Simon Breiter
