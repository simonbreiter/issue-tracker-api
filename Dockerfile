FROM node:7

MAINTAINER Emanuele Mazzotta hello@mazzotta.me

RUN mkdir -p /usr/src/issue-tracker-api
WORKDIR /usr/src/issue-tracker-api
ADD package.json /usr/src/issue-tracker-api/package.json

RUN npm install && \
    npm install -g nodemon

ADD . /usr/src/issue-tracker-api
