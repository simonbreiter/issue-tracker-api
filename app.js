let mongoose = require('mongoose')
let express = require('express')
let app = express()
let morgan = require('morgan')
let db = mongoose.connection
let bodyParser = require('body-parser')
let cors = require('cors')
const config = require('./config')
const api_port = process.env.API_PORT || 3000
const mongodb_host = process.env.MONGODB_HOST || 'localhost'

// Setup and connect to database
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
    console.log("Successfully connected to Database!")
})
mongoose.connect('mongodb://' + mongodb_host + '/issue-tracker') // Connect to database

app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

app.use(cors())

// Use morgan to log requests to the console
app.use(morgan('dev'))

app.use(require('./controllers'))

app.listen(api_port, function () {
    console.log('App listen on Port ' + api_port)
})
