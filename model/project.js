let mongoose = require('mongoose')
let Schema = mongoose.Schema

let ProjectSchema = Schema({
    name: {type: String, required: true},
    url: {type: String, required: true}
})

module.exports = mongoose.model('Project', ProjectSchema)
