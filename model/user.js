let mongoose = require('mongoose')
let Schema = mongoose.Schema

let UserSchema = Schema({
    name: String,
    password: String,
    admin: Boolean,
    projects: Array
})

module.exports = mongoose.model('User', UserSchema)
