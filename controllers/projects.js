let mongoose = require('mongoose')
let express = require('express')
let router = express.Router()
let Project = require('../model/project')
let User = require('../model/user')
let app = express()
let bodyParser = require('body-parser')

// Use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

router.route('/users/:user_id/projects')
// route that create a project for the user with id (accessed at POST http://localhost:3000/api/users/{user_id}/projects)
    .post(function (req, res) {
        let project = new Project()
        project.name = req.body.name
        project.url = req.body.url

        // Add project to user
        User.findById(req.params.user_id, function (err, user) {

            user.projects.push(project._id)

            user.save(function (err) {
                if (err) {
                    res.send(err)
                }
            })
        })

        // Save new project
        project.save(function (err) {
            if (err) {
                res.send(err)
            }
            res.json({message: 'Feed created!'})
        })
    })
    // route that get all the projects of user with this id (accessed at GET http://localhost:3000/api/users/{user_id}/projects)
    .get(function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            if (err) {
                res.send(err)
            }
            Project.find({"_id": {$in: user.projects}}, function (err, projects) {
                if (err) {
                    res.send(err)
                }
                res.json(projects)
            })
        })

    })

router.route('/users/:user_id/projects/:project_id')
// route that get the project with that id of user with that id (accessed at GET http://localhost:3000/api/users/{user_id}/projects/{project_id})
    .get(function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            if (err) {
                res.send(err)
            }
            if(user.projects.includes(req.params.project_id)) {
                Project.findById(req.params.project_id, function (err, projects) {
                    if (err) {
                        res.send(err)
                    }
                    res.json(projects)
                })
            }
            else {
                res.json({message: 'This user has no project with this ID.'})
            }
        })
    })
    // route that update the project with this id of user with that id (accessed at PUT http://localhost:3000/api/users/{user_id}/projects/{project_id})
    .put(function (req, res) {

        User.findById(req.params.user_id, function (err, user) {
            if (err) {
                res.send(err)
            }

            if(user.projects.includes(req.params.project_id)) {
                // use our project model to find the project we want
                Project.findById(req.params.project_id, function (err, project) {

                    if (err) {
                        res.send(err)
                    }

                    if (req.body.name) {
                        project.name = req.body.name  // update the project name
                    }

                    if (req.body.url) {
                        project.url = req.body.url  // update the project url
                    }

                    // save the project
                    project.save(function (err) {
                        if (err) {
                            res.send(err)
                        }
                        res.json({message: 'Feed updated!'})
                    })

                })
            }
            else {
                res.json({message: 'This user has no project with this ID.'})
            }
        })

    })
    // route that delete the project with this id of user with this id (accessed at DELETE http://localhost:3000/api/projects/:name)
    .delete(function (req, res) {
        User.findById(req.params.user_id, function (err, user) {
            if (err) {
                res.send(err)
            }
            if(user.projects.includes(req.params.project_id)) {
                // remove project from user
                User.update(
                    {
                        _id : req.params.user_id
                    }, {
                        $pull: {
                            'projects': req.params.project_id
                        }
                    }, function(err, user) {
                        if (err) {
                            res.send(err)
                        }
                        // remove document
                        Project.remove({
                            _id : req.params.project_id
                        }, function (err, project) {
                            if (err) {
                                res.send(err)
                            }
                            res.json({message: 'Successfully deleted'})
                        })
                    })
            }
            else {
                res.json({message: 'This user has no project with this ID.'})
            }
        })
    })

module.exports = router
