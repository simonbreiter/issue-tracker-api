let express = require('express')
let router = express.Router()

router.route('/api')
    .get(function (req, res) {
        res.json({message: 'Welcome to issue-tracker-api!'})
    })

router.use('/api/', require('./auth'))
router.use('/api/', require('./users'))
router.use('/api/', require('./projects'))

module.exports = router